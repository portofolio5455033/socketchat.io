
const { profile } = require('console');
const express = require('express');
const app = express();
const path = require('path')

const http = require('http').Server(app);
const port = process.env.PORT||8080;

// attached http server to the socket.io
const io = require("socket.io")(http);


// route
app.get('/',(req,res) => {
    res.sendFile(path.join(__dirname, 'src/index.html'))
})

// app.get('/',(req,res) => {
//     res.json("get Reqeust");
// })

// create a new connection
io.on('connection', socket => {
    console.log('A user connected');

    // menangani perubahan data profil dari client
    socket.on('update profile', profile => {
        console.log('update profile' + profile);
        // mengirim data profil baru ke semua client yang terkoneksi
        io.emit('profile updated', profile);
    });

    socket.on('disconnect', () => {
        console.log('A user disconnected');
    })

socket.on("message", msg => {
    console.log("Client message :" + msg);
})

// dari server ke client
// emit event
socket.emit("server", "Receive From Server");
socket.emit("server", "Receive From Server1");

})

// app.listen(port, () => {
//     console.log('App listening on port', port)
// })

http.listen(port, () => {
    console.log('App listening on port', port)
})

